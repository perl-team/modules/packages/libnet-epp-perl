Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Gavin Brown <gbrown@cpan.org>
Source: https://metacpan.org/release/Net-EPP
Upstream-Name: Net-EPP

Files: *
Copyright: 2008-2023, CentralNic Ltd <http://www.centralnic.com>
 2024, Gavin Brown <gbrown@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2009-2011, Jonathan Yu <jawnsy@cpan.org>
 2009, Peter Pentchev <roam@ringlet.net>
 2011, Fabrizio Regalli <fabreg@fabreg.it>
 2012-2016, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'
